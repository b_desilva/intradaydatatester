package com.dfn.shared;

import com.opencsv.CSVWriter;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileSystemManager {


    private static final String DATA_DOWNLOAD_FILE_NAME = "dataFiles";
    private static final String DATA_GENERATE_FILE_NAME = "generateFiles";
    private static final String SYMBOL_MAPPINGS_FILE_NAME = "symbolmapping.csv";
    private static final String HISTORY_FOLDER_NAME = "history";
    private static final String INTRADAY_FOLDER_NAME = "ohlc";
    private static final String SYMBOL_MAPPINGS = "symbolMappings";

    private static final String AVERAGE_FOLDER = "averages";
    private static final String VWAP = "vwap";
    private static final String VWAP_SUMMERY = "vwapSummery";
    private static final String VWAP_LAST_CROSS= "vwapLastCross";


    private static String INITIAL_FILE_PATH;
    private static String GENERATE_FILE_PATH;
    private static String HISTORY_FOLDER_PATH;
    private static String INTRADAY_FOLDER_PATH;
    private static String SYMBOL_MAPPING_FOLDER_PATH;

    static {
        INITIAL_FILE_PATH = "." + System.getProperty("file.separator") + DATA_DOWNLOAD_FILE_NAME;
        GENERATE_FILE_PATH = "." + System.getProperty("file.separator") + DATA_GENERATE_FILE_NAME;
        HISTORY_FOLDER_PATH = INITIAL_FILE_PATH + System.getProperty("file.separator") + HISTORY_FOLDER_NAME;
        INTRADAY_FOLDER_PATH = INITIAL_FILE_PATH + System.getProperty("file.separator") + INTRADAY_FOLDER_NAME;
        SYMBOL_MAPPING_FOLDER_PATH = INITIAL_FILE_PATH + System.getProperty("file.separator") + SYMBOL_MAPPINGS;
    }

    private static FileSystemManager self;


    private FileSystemManager() {
    }

    public static FileSystemManager getSharedInstance() {
        if (self == null) {
            self = new FileSystemManager();
        }
        return self;
    }

    public static String getHistoryFolderPathWithExchange(String exchange) {
        return HISTORY_FOLDER_PATH + System.getProperty("file.separator") + exchange;
    }

    public static String getIntradayFolderPathWithExchange(String exchange) {
        return INTRADAY_FOLDER_PATH + System.getProperty("file.separator") + exchange;
    }

    public static String getSymbolMappingFilePath(String exchange) {
        return SYMBOL_MAPPING_FOLDER_PATH + System.getProperty("file.separator") + exchange;
    }

    public static String getGenerateFileExchangePath(String exchange) {
        return GENERATE_FILE_PATH + System.getProperty("file.separator") + exchange;
    }

    public static String getAveragesFilePath(String exchange) {
        return GENERATE_FILE_PATH + System.getProperty("file.separator") + exchange + System.getProperty("file.separator")
                + AVERAGE_FOLDER;
    }

    public static String getVwapFilePath(String exchange) {
        return GENERATE_FILE_PATH + System.getProperty("file.separator") + exchange + System.getProperty("file.separator")
                + VWAP;
    }

    public static String getVwapSummeryFilePath(String exchange) {
        return GENERATE_FILE_PATH + System.getProperty("file.separator") + exchange + System.getProperty("file.separator")
                + VWAP_SUMMERY;
    }

    public static String getVwapLastCrossFilePath(String exchange) {
        return GENERATE_FILE_PATH + System.getProperty("file.separator") + exchange + System.getProperty("file.separator")
                + VWAP_LAST_CROSS;
    }

    public static void clearAllGenerateFolders(){
        try {
            File generatePath = new File(GENERATE_FILE_PATH);
            FileUtils.cleanDirectory(generatePath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void createExchangeFolders(String exchange) {

        File generatePath = new File(GENERATE_FILE_PATH);
        if (!generatePath.exists()) {
            generatePath.mkdirs();
        }

        File generateExchangePath = new File(getGenerateFileExchangePath(exchange));
        if (!generateExchangePath.exists()) {
            generateExchangePath.mkdirs();
        }

        File generateExchangeAveragePath = new File(getAveragesFilePath(exchange));
        if (!generateExchangeAveragePath.exists()) {
            generateExchangeAveragePath.mkdirs();
        }

        File generateExchangeVWAPPath = new File(getVwapFilePath(exchange));
        if (!generateExchangeVWAPPath.exists()) {
            generateExchangeVWAPPath.mkdirs();
        }

        File generateExchangeVWAPSummeryPath = new File(getVwapSummeryFilePath(exchange));
        if (!generateExchangeVWAPSummeryPath.exists()) {
            generateExchangeVWAPSummeryPath.mkdirs();
        }

        File generateExchangeVWAPLastCrossPath = new File(getVwapLastCrossFilePath(exchange));
        if (!generateExchangeVWAPLastCrossPath.exists()) {
            generateExchangeVWAPLastCrossPath.mkdirs();
        }
    }


    public static String getSymbolMappingsFileName() {
        return SYMBOL_MAPPINGS_FILE_NAME;
    }

    public static List<String> getIntradayFolderList(String exchange) {
        String path = getIntradayFolderPathWithExchange(exchange);
        return getSortedDirectoryNames(path);
    }

    public static List<String> getHistoryFolderList(String exchange) {
        String path = getHistoryFolderPathWithExchange(exchange);
        return getSortedDirectoryNames(path);
    }


    private static List<String> getSortedDirectoryNames(String path) {
        List<String> list = new ArrayList<>();
        try {
            File file = new File(path);
            String[] directories;
            directories = file.list(new FilenameFilter() {
                @Override
                public boolean accept(File current, String name) {
                    return new File(current, name).isDirectory();
                }
            });

            if (directories != null) {
                list = (Arrays.asList(directories));
                list.sort(Comparator.reverseOrder());
            }
        } catch (Exception e) {
        }
        return list;
    }

    public static List<String> getCSVFileNamesList(String path) {
        try (Stream<Path> pathStream = Files.list(Paths.get(path))) {
            return pathStream.filter(s -> s.toString().endsWith(".csv")).collect(Collectors.toList()).stream().map(f -> f.getFileName().toString()).collect(Collectors.toCollection(() -> new ArrayList<>()));
        } catch (Exception e) {
        }
        return new ArrayList<>();
    }

    public static List<String> getRowListOfFile(String path) {
        try {
            Path filePath = Paths.get(path);
            return Files.readAllLines(filePath).stream().skip(1).collect(Collectors.toList()); //skip(1) for remove the header
        } catch (Exception e) {
        }
        return new ArrayList<>();
    }

    public static String removeExtension(String fileName) {
        if (fileName.indexOf(".") > 0) {
            return fileName.substring(0, fileName.lastIndexOf("."));
        } else {
            return fileName;
        }
    }

    private static void writeCSV(String path, String[] header, List<String[]> data) {
        // first create file object for file placed at location
        // specified by filepath
        File file = new File(path);
        try {
            // create FileWriter object with file as parameter
            FileWriter outputfile = new FileWriter(file);

            // create CSVWriter object filewriter object as parameter
            CSVWriter writer = new CSVWriter(outputfile);

            writer.writeNext(header);

            writer.writeAll(data);

            // closing writer connection
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeAverageCSV(String exchange, String symbol, String[] header, List<String[]> data) {
        String path = getAveragesFilePath(exchange) + System.getProperty("file.separator") + symbol + ".csv";
        writeCSV(path, header, data);
    }

    public static void writeVWAPCSV(String exchange, String symbol, String[] header, List<String[]> data) {
        String path = getVwapFilePath(exchange) + System.getProperty("file.separator") + symbol + ".csv";
        writeCSV(path, header, data);
    }

    public static void writeVWAPSummeryCSV(String exchange, String symbol, String[] header, List<String[]> data) {
        String path = getVwapSummeryFilePath(exchange) + System.getProperty("file.separator") + symbol + ".csv";
        writeCSV(path, header, data);
    }

    public static void writeVWAPLastCrossCSV(String exchange, String[] header, List<String[]> data) {
        String path = getVwapLastCrossFilePath(exchange) + System.getProperty("file.separator") + "VwapLastCross" + ".csv";
        writeCSV(path, header, data);
    }


}
