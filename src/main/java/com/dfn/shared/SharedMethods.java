package com.dfn.shared;

import com.dfn.store.object.Candle;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 *
 */
public class SharedMethods {

    private static SharedMethods self;

    private static SimpleDateFormat dateFormat;
    private static SimpleDateFormat dateTimeFormat;

    public static final String CROSSES_OVER = "Crosses Over";
    public static final String CROSSES_BELLOW = "Crosses Bellow";
    public static final String ABOVE_PRICE_LINE = "Above Price Line";
    public static final String BELLOW_PRICE_LINE = "Bellow Price Line";
    public static final String EQUAL = "Equal";

    public static final TimeZone saudiTimeZone = TimeZone.getTimeZone("Asia/Riyadh"); //Target timezone
    public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    public static final SimpleDateFormat SIMPLE_DATE_ONLY_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    public static final SimpleDateFormat SIMPLE_TIME_FORMAT = new SimpleDateFormat("HH:mm");

    public static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#,##0.00");
    public static final DecimalFormat LONG_FORMAT = new DecimalFormat("#,##0");

    static {
        dateFormat = new SimpleDateFormat("yyyyMMdd");
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

        dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        dateTimeFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

        SIMPLE_DATE_FORMAT.setTimeZone(saudiTimeZone);
        SIMPLE_DATE_ONLY_FORMAT.setTimeZone(saudiTimeZone);
        SIMPLE_TIME_FORMAT.setTimeZone(saudiTimeZone);

        }


    private SharedMethods() {
    }

    public static SharedMethods getSharedInstance() {
        if (self == null) {
            self = new SharedMethods();
        }
        return self;
    }

    public InputStream getInputStream(String path) {
        try {
            FileInputStream fileInputStream = new FileInputStream(path);
            return fileInputStream;
        } catch (FileNotFoundException e) {
            return null;
        }

    }

    public static boolean isNullOrEmptyString(String string) {
        return string == null || string.trim().isEmpty();
    }

    public static List<String> getExchangeList() {
        ArrayList<String> exchangeList = new ArrayList<>();
        try {
            String list = "TDWL";
            String[] ex = list.split(",");
            Collections.addAll(exchangeList, ex);
        } catch (Exception e) {
        }
        return exchangeList;
    }

    public static long stringDateToLong(String date) {
        try {
            Date d = dateFormat.parse(date);
            return d.getTime();
        } catch (Exception e) {
        }
        return 0L;
    }

    public static long stringDateTimeToLong(String dateTime) {
        try {
            Date d = dateTimeFormat.parse(dateTime);
            return d.getTime();
        } catch (Exception e) {
        }
        return 0L;
    }

    public static String getVwapCorss(Candle current, Candle oldCandle) {
        if (oldCandle != null && (oldCandle.getVwap() <= oldCandle.getClose()) && (current.getVwap() > current.getClose())) {
            return CROSSES_OVER;
        } else if (oldCandle != null && (oldCandle.getVwap() > oldCandle.getClose()) && (current.getVwap() <= current.getClose())) {
            return CROSSES_BELLOW;
        } else if (current.getVwap() > current.getClose()) {
            return ABOVE_PRICE_LINE;
        } else if (current.getClose() > current.getVwap()) {
            return BELLOW_PRICE_LINE;
        } else {
            return EQUAL;
        }
    }

}
