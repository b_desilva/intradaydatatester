package com.dfn.dataWriter;

import com.dfn.cal.CalculationUtilsStore;
import com.dfn.cal.CumulativeAvgCandle;
import com.dfn.cal.VWAPCandle;
import com.dfn.shared.FileSystemManager;
import com.dfn.shared.SharedMethods;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CSVDataWriter {

    public static CSVDataWriter self;

    private CSVDataWriter() {
    }

    public static CSVDataWriter getSharedInstance() {
        if (self == null) {
            self = new CSVDataWriter();
        }
        return self;
    }

    public void writeAverageVolumeTrade(String exchange, int dayCount) {
        String[] header = new String[]{"TIME", "AVERAGE VOLUME", "AVERAGE TURNOVER", "AVERAGE TRADES", "VOLUME TRADE RATIO"};
        Map<String, List<CumulativeAvgCandle>> map = CalculationUtilsStore.getSharedInstance().getAverageCumulativeData(exchange, dayCount);

        Set<String> symbols = map.keySet();

        for (String sym : symbols) {
            List<CumulativeAvgCandle> list = map.get(sym);
            if (list.size() > 0) {
                List<String[]> dataList = new ArrayList<>();
                for (CumulativeAvgCandle cumulativeAvgCandle : list) {
                    dataList.add(cumulativeAvgCandle.getValue());
                }

                FileSystemManager.writeAverageCSV(exchange, sym, header, dataList);
            }

        }
    }

    public void writeVwapCrosses(String exchange) {
        String[] header = new String[]{"TIME", "CLOSE", "VWAP", "VOLUME", "TRADES", "CUMULATIVE VOLUME", "CUMULATIVE TRADES", "VWAP STATUS"};
        Map<String, List<VWAPCandle[]>> vwapMap = CalculationUtilsStore.getSharedInstance().getVwapCross(exchange);

        Set<String> symbols = vwapMap.keySet();

        for (String sym : symbols) {
            List<VWAPCandle[]> list = vwapMap.get(sym);
            if (list.size() > 0) {

                List<String[]> dataList = new ArrayList<>();
                List<String[]> crossDataList = new ArrayList<>();

                for (VWAPCandle[] vwapCandles : list) {

                    for (int j = vwapCandles.length - 1; j > 0; j--) {
                        if (vwapCandles[j] != null) {
                            dataList.add(vwapCandles[j].getValue());

                            if (SharedMethods.CROSSES_OVER.equals(vwapCandles[j].getVwapCross()) || SharedMethods.CROSSES_BELLOW.equals(vwapCandles[j].getVwapCross())) {
                                crossDataList.add(vwapCandles[j].getValue());
                            }
                        }
                    }

                    dataList.add(new String[]{"", "", "", "", "", "", "", ""}); // Empty row
                    crossDataList.add(new String[]{"", "", "", "", "", "", "", ""}); // Empty row
                }

                FileSystemManager.writeVWAPCSV(exchange, sym, header, dataList);
                FileSystemManager.writeVWAPSummeryCSV(exchange, sym, header, crossDataList);
            }

        }
    }

    public void writeVwapLastCross(String exchange) {

        String[] header = new String[]{"SYMBOL", "DATE", "LAST_CROSS_TIME","LAST_CROSS_TIME_MILLI", "CROSSING_VWAP", "CLOSING_VWAP", "LAST_CLOSE","DIFF"};
        Map<String, List<VWAPCandle[]>> vwapMap = CalculationUtilsStore.getSharedInstance().getVwapCross(exchange);

        Set<String> symbols = vwapMap.keySet();
        List<String[]> dataList = new ArrayList<>();

        for (String sym : symbols) {

            List<VWAPCandle[]> list = vwapMap.get(sym);
            if (list.size() > 0) {

                for (VWAPCandle[] vwapCandles : list) {

                    for (int j = vwapCandles.length - 1; j > 0; j--) {
                        if (vwapCandles[j] != null) {
                            if (SharedMethods.CROSSES_OVER.equals(vwapCandles[j].getVwapCross()) || SharedMethods.CROSSES_BELLOW.equals(vwapCandles[j].getVwapCross())) {
                                dataList.add(vwapCandles[j].getLastCrossValue());
                                break;
                            }
                        }
                    }
                }
            }

        }

        FileSystemManager.writeVWAPLastCrossCSV(exchange, header, dataList);
    }
}
