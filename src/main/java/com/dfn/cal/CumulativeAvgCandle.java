package com.dfn.cal;

import com.dfn.shared.SharedMethods;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CumulativeAvgCandle {

    private long time;
    private long cumuAvgVol;
    private int cumnAvgTrade;
    private double cumnAvgTurnover;


    public CumulativeAvgCandle(long time, long cumuAvgVol, int cumnAvgTrade, double cumnAvgTurnover) {
        this.time = time;
        this.cumuAvgVol = cumuAvgVol;
        this.cumnAvgTrade = cumnAvgTrade;
        this.cumnAvgTurnover = cumnAvgTurnover;
    }

    public long getTime() {
        return time;
    }

    public long getCumuAvgVol() {
        return cumuAvgVol;
    }

    public int getCumnAvgTrade() {
        return cumnAvgTrade;
    }

    public double getCumnAvgTurnover() {
        return cumnAvgTurnover;
    }


    public String[] getValue(){
        return new String[] {SharedMethods.SIMPLE_TIME_FORMAT.format(new Date(this.time)),SharedMethods.LONG_FORMAT.format(cumuAvgVol),
                SharedMethods.DECIMAL_FORMAT.format(cumnAvgTurnover),SharedMethods.LONG_FORMAT.format(cumnAvgTrade),SharedMethods.DECIMAL_FORMAT.format(cumuAvgVol/cumnAvgTrade)};
    }

}
