package com.dfn.cal;

public class VWAPLastCrossCandle {

    private String symbol;
    private long time;
    private double crossingVwap;
    private double closingVwap;
    private double closePrice;

    public VWAPLastCrossCandle(String symbol, long time, double crossingVwap, double closingVwap, double closePrice) {
        this.symbol = symbol;
        this.time = time;
        this.crossingVwap = crossingVwap;
        this.closingVwap = closingVwap;
        this.closePrice = closePrice;
    }

    public String getSymbol() {
        return symbol;
    }

    public long getTime() {
        return time;
    }

    public double getCrossingVwap() {
        return crossingVwap;
    }

    public double getClosingVwap() {
        return closingVwap;
    }

    public double getClosePrice() {
        return closePrice;
    }
}
