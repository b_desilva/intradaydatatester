package com.dfn.cal;

import com.dfn.shared.SharedMethods;
import com.dfn.store.OHLCHistoryDataStore;
import com.dfn.store.OHLCIntradayDataStore;
import com.dfn.store.object.Candle;

import java.util.*;

public class CalculationUtilsStore {

    private static CalculationUtilsStore self;

    private CalculationUtilsStore() {
    }

    public static CalculationUtilsStore getSharedInstance() {
        if (self == null) {
            self = new CalculationUtilsStore();
        }
        return self;
    }

    public Map<String, List<CumulativeAvgCandle>> getAverageCumulativeData(String exchange, int days) {

        Map<String, List<CumulativeAvgCandle>> cumuCandle = new HashMap<>();
        Map<String, List<CumulativeCandle[]>> map = CumulativeDataStore.getSharedInstance().getIntradayOHLCIndexIntardayList(exchange);
        Set<String> symbolList = map.keySet();

        for (String symbol : symbolList) {
            List<CumulativeCandle[]> list = map.get(symbol);
            if (list.size() > days) {
                List<CumulativeCandle[]> dayList = list.subList(0, days);
                List<CumulativeAvgCandle> cumulativeAvgCandleList = new ArrayList<>();

                Map<Integer, Integer> lastAvailableIndex = new HashMap<>(); // day num -> available index

                for (int j = 0; j < 24 * 60; j++) {

                    long time = 0;
                    long totalVolume = 0;
                    int totalTrades = 0;
                    double totalTurnover = 0;

                    long avgTotalVolume = 0;
                    int avgTotalTrades = 0;
                    double avgTotalTurnover = 0;

                    CumulativeCandle cumulativeCandle = null;

                    for (int day = 0; day < days; day++) {
                        if (dayList.get(day)[j] != null) {
                            time = dayList.get(day)[j].getTime();
                            totalTrades += dayList.get(day)[j].getCumnTrade();
                            totalVolume += dayList.get(day)[j].getCumuVol();
                            totalTurnover += dayList.get(day)[j].getCumnTurnover();
                            lastAvailableIndex.put(day, j);
                        } else {
                            if (lastAvailableIndex.containsKey(day)) {
                                int lastAvailable = lastAvailableIndex.get(day);
                                if (lastAvailable > 0) {
                                    totalTrades += dayList.get(day)[lastAvailable].getCumnTrade();
                                    totalVolume += dayList.get(day)[lastAvailable].getCumuVol();
                                    totalTurnover += dayList.get(day)[lastAvailable].getCumnTurnover();
                                }
                            }
                        }
                    }

                    avgTotalVolume = totalVolume / days;
                    avgTotalTrades = (int) (totalTrades / days);
                    avgTotalTurnover = totalTurnover / days;

                    if (time > 0 && avgTotalVolume > 0 && avgTotalTrades > 0 && avgTotalTurnover > 0) {
                        CumulativeAvgCandle cumulativeAvgCandle = new CumulativeAvgCandle(time, avgTotalVolume, avgTotalTrades, avgTotalTurnover);
                        cumulativeAvgCandleList.add(cumulativeAvgCandle);
                    } else {
                        if (time > 0) {
                            System.out.println("...Bad record come for CumulativeAvgCandle--" + symbol + "--Time--" + new Date(time) + "--avgTotalVolume--" + avgTotalVolume + "--avgTotalTrades--" + avgTotalTrades);
                        }
                    }
                }
                cumuCandle.put(symbol, cumulativeAvgCandleList);
            } else {
                System.out.println("..symbol." + symbol + "..not having enough records for Calculating the Average");
            }
        }

        return cumuCandle;
    }

    public Map<String, List<VWAPCandle[]>> getVwapCross(String exchange) {
        Map<String, List<Candle[]>> map = OHLCIntradayDataStore.getSharedInstance().getIntradayExchangeOHLCIndexIntardayList(exchange);

        Map<String, List<VWAPCandle[]>> vwapMap = new HashMap<>();

        Set<String> symbolList = map.keySet();

        for (String sym : symbolList) {

            List<Candle[]> candles = map.get(sym);
            List<VWAPCandle[]> vwapCandlesList = new ArrayList<>();


            for (Candle[] candle : candles) {
                Candle candle1 = null;
                Candle preCandle = null;
                Candle historyCandle = null;

                VWAPCandle[] vwapCandlesArray = new VWAPCandle[candle.length];

                long cumulativeVolume = 0;
                int cumulativeTrades = 0;

                for (int j = 0; j < candle.length; j++) {

                    if (candle[j] != null) {

                        candle1 = candle[j];
                        String crossStatus = SharedMethods.getVwapCorss(candle1, preCandle);
                        double close = candle1.getClose();
                        double vwap = candle1.getVwap();
                        long time = candle1.getTime();
                        long volume = candle1.getVolume();
                        int trades = candle1.getNoTrades();
                        double lastTrade = 0;
                        double lastVwap = 0;

                        // Get Last Trade Price and Closing VWAP from the History data store.
                        if (historyCandle == null) {

                            Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
                            calendar.setTime(new Date(time));
                            calendar.set(Calendar.HOUR_OF_DAY, 0);
                            calendar.set(Calendar.MINUTE, 0);
                            calendar.set(Calendar.SECOND, 0);
                            calendar.set(Calendar.MILLISECOND, 0);
                            long date = calendar.getTimeInMillis();

                            historyCandle = OHLCHistoryDataStore.getSharedInstance().getCandle(exchange, sym, date);
                        }

                        if (historyCandle != null) {
                            lastTrade = historyCandle.getClose();
                            lastVwap = (historyCandle.getTurnover() / historyCandle.getVolume());
                        }

                        cumulativeVolume += candle1.getVolume();
                        cumulativeTrades += candle1.getNoTrades();


                        VWAPCandle vwapCandle = new VWAPCandle(sym,time, close, vwap, volume, trades, cumulativeVolume, cumulativeTrades, lastVwap, lastTrade, crossStatus);
                        vwapCandlesArray[j] = vwapCandle;

                        preCandle = candle1;
                    }

                }
                vwapCandlesList.add(vwapCandlesArray);
            }
            vwapMap.put(sym, vwapCandlesList);
        }
        return vwapMap;
    }


}
