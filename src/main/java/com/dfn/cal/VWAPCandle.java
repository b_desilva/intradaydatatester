package com.dfn.cal;

import com.dfn.shared.SharedMethods;

import java.text.SimpleDateFormat;
import java.util.Date;

public class VWAPCandle {

    private String sym;
    private long time;
    private double closePrice;
    private double vwap;
    private long volume;
    private int trades;
    private long cumulativeVolume;
    private int cumulativeTrades;
    private String vwapCross;
    private double lastVwap;
    private double lastPrice;


    public VWAPCandle(String sym, long time, double closePrice, double vwap, long volume, int trades, long cumulativeVolume, int cumulativeTrades, double lastVwap, double lastPrice, String vwapCross) {
        this.sym = sym;
        this.time = time;
        this.closePrice = closePrice;
        this.vwap = vwap;
        this.volume = volume;
        this.trades = trades;
        this.cumulativeVolume = cumulativeVolume;
        this.cumulativeTrades = cumulativeTrades;
        this.lastVwap = lastVwap;
        this.lastPrice = lastPrice;
        this.vwapCross = vwapCross;
    }

    public String getSym() {
        return sym;
    }

    public long getTime() {
        return time;
    }

    public double getClosePrice() {
        return closePrice;
    }

    public double getVwap() {
        return vwap;
    }

    public String getVwapCross() {
        return vwapCross;
    }

    public long getVolume() {
        return volume;
    }

    public int getTrades() {
        return trades;
    }

    public long getCumulativeVolume() {
        return cumulativeVolume;
    }

    public int getCumulativeTrades() {
        return cumulativeTrades;
    }

    public double getLastVwap() {
        return lastVwap;
    }

    public double getLastPrice() {
        return lastPrice;
    }

    public String[] getHeader() {
        return new String[]{"TIME", "CLOSE", "VWAP", "VOLUME", "TRADES", "CUMULATIVE VOLUME", "CUMULATIVE TRADES", "VWAP STATUS"};
    }

    public String[] getValue() {
        return new String[]{SharedMethods.SIMPLE_DATE_FORMAT.format(new Date(this.time)), SharedMethods.DECIMAL_FORMAT.format(closePrice), SharedMethods.DECIMAL_FORMAT.format(vwap), SharedMethods.LONG_FORMAT.format(volume), SharedMethods.LONG_FORMAT.format(trades),
                SharedMethods.LONG_FORMAT.format(cumulativeVolume), SharedMethods.LONG_FORMAT.format(cumulativeTrades), String.valueOf(vwapCross)};
    }

    public String[] getLastCrossValue() {
        Date orginal = new Date(this.time);
        orginal.setYear(1);
        orginal.setMonth(1);
        orginal.setDate(1);
        if (lastVwap > 0) {
            return new String[]{this.sym, SharedMethods.SIMPLE_DATE_ONLY_FORMAT.format(new Date(this.time)), SharedMethods.SIMPLE_TIME_FORMAT.format(new Date(this.time)), String.valueOf(orginal.getTime() * -1), SharedMethods.DECIMAL_FORMAT.format(vwap)
                    , SharedMethods.DECIMAL_FORMAT.format(lastVwap), SharedMethods.DECIMAL_FORMAT.format(lastPrice),SharedMethods.DECIMAL_FORMAT.format(lastVwap - vwap)};
        }
        return new String[1];
    }
}
