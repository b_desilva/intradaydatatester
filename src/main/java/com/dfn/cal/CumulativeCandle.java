package com.dfn.cal;

public class CumulativeCandle {

    private long time;
    private long cumuVol;
    private int cumnTrade;
    private double cumnTurnover;
    private double vwap;

    public CumulativeCandle(long time, long cumuVol, int cumnTrade, double cumnTurnover, double vwap) {
        this.time = time;
        this.cumuVol = cumuVol;
        this.cumnTrade = cumnTrade;
        this.cumnTurnover = cumnTurnover;
        this.vwap = vwap;
    }

    public long getTime() {
        return time;
    }

    public long getCumuVol() {
        return cumuVol;
    }

    public int getCumnTrade() {
        return cumnTrade;
    }

    public double getCumnTurnover() {
        return cumnTurnover;
    }

    public double getVwap() {
        return vwap;
    }
}
