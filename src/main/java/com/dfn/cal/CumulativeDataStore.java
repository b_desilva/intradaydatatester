package com.dfn.cal;

import com.dfn.store.object.Candle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CumulativeDataStore {

    private Map<String, Map<String, List<CumulativeCandle[]>>> candleIndexDataList; //exchange-> symbol-> Candle index List // 1 min store
    private static CumulativeDataStore self;

    public CumulativeDataStore() {
        this.candleIndexDataList = new HashMap<>();
    }

    public static CumulativeDataStore getSharedInstance() {
        if (self == null) {
            self = new CumulativeDataStore();
        }
        return self;
    }

    public void addIntradayOHLCIndexArray(String exchange, String symbol, CumulativeCandle[] candle) {

        if (candleIndexDataList.containsKey(exchange)) {
            Map<String, List<CumulativeCandle[]>> symbolMap = candleIndexDataList.get(exchange);
            if (symbolMap.containsKey(symbol)) {
                List<CumulativeCandle[]> candleList = symbolMap.get(symbol);
                candleList.add(candle);
            } else {
                List<CumulativeCandle[]> candleList = new ArrayList<>();
                candleList.add(candle);
                symbolMap.put(symbol, candleList);
            }

        } else {
            HashMap<String, List<CumulativeCandle[]>> symbolMap = new HashMap<>();
            List<CumulativeCandle[]> candleList = new ArrayList<>();
            candleList.add(candle);
            symbolMap.put(symbol, candleList);
            candleIndexDataList.put(exchange, symbolMap);
        }
    }

    public Map<String, List<CumulativeCandle[]>> getIntradayOHLCIndexIntardayList(String exchange) {
        if (candleIndexDataList.containsKey(exchange)) {
            return candleIndexDataList.get(exchange);
        }
        return new HashMap<String, List<CumulativeCandle[]>>();
    }

}
