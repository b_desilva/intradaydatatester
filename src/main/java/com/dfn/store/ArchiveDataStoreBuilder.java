package com.dfn.store;

import com.dfn.cal.CumulativeCandle;
import com.dfn.cal.CumulativeDataStore;
import com.dfn.shared.FileSystemManager;
import com.dfn.shared.SharedMethods;
import com.dfn.store.object.Candle;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ArchiveDataStoreBuilder {

    private static ArchiveDataStoreBuilder self;

    private ArchiveDataStoreBuilder() {

    }

    public static ArchiveDataStoreBuilder getSharedInstance() {
        if (self == null) {
            self = new ArchiveDataStoreBuilder();
        }
        return self;
    }

    public void startStoreBuilder() {
        buildTheSymbolMap();
        loadIntradayOHLCData();
        loadHistoryOHLCData();
    }


    private void loadIntradayOHLCData() {

        SharedMethods.getExchangeList().forEach(exchange -> {
            List<String> folderList = FileSystemManager.getIntradayFolderList(exchange);

            folderList.forEach(folderName -> {
                String folderPath = FileSystemManager.getIntradayFolderPathWithExchange(exchange) + System.getProperty("file.separator") + folderName;
                List<String> csvFiles = FileSystemManager.getCSVFileNamesList(folderPath);

                Calendar calendar = Calendar.getInstance();

                csvFiles.forEach(csvFile -> {
                    try {
                        Candle[] candleIndexArray = new Candle[60 * 24];
                        CumulativeCandle[] cumnCandleIndexArray = new CumulativeCandle[60 * 24];

                        final long[] cumnVolume = {0};
                        final double[] cumnTurnover = {0};
                        final int[] cumnTrades = {0};


                        String csvPath = folderPath + System.getProperty("file.separator") + csvFile;
                        String mappingSymbol = FileSystemManager.removeExtension(csvFile);
                        String symbol = SymbolMappingStore.getSharedInstance().getSymbol(exchange, mappingSymbol);
                        List<String> lines = FileSystemManager.getRowListOfFile(csvPath);

                        lines.forEach(line -> {
                            try {
                                if (!SharedMethods.isNullOrEmptyString(symbol)) {
                                    String[] dataAry = line.split("\\|");
                                    Candle candle = new Candle(symbol, intradayMilliseconds(dataAry[1]), Double.parseDouble(dataAry[2]), Double.parseDouble(dataAry[3]), Double.parseDouble(dataAry[4]), Double.parseDouble(dataAry[5]), Long.parseLong(dataAry[6]), Double.parseDouble(dataAry[7]), Integer.parseInt(dataAry[8]), Double.parseDouble(dataAry[9]));
                                    OHLCIntradayDataStore.getSharedInstance().addIntradayOHLCObject(exchange, symbol, candle);

                                    calendar.setTime(new Date(candle.getTime()));
                                    int hours = calendar.get(Calendar.HOUR_OF_DAY);
                                    int minutes = calendar.get(Calendar.MINUTE);

                                    candleIndexArray[(hours * 60) + minutes] = candle;

                                    cumnVolume[0] += candle.getVolume();
                                    cumnTurnover[0] += candle.getTurnover();
                                    cumnTrades[0] += candle.getNoTrades();

                                    CumulativeCandle cumulativeCandle = new CumulativeCandle(candle.getTime(), cumnVolume[0], cumnTrades[0], cumnTurnover[0], candle.getVwap());
                                    cumnCandleIndexArray[(hours * 60) + minutes] = cumulativeCandle;
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        });

                        OHLCIntradayDataStore.getSharedInstance().addIntradayOHLCIndexArray(exchange, symbol, candleIndexArray);
                        CumulativeDataStore.getSharedInstance().addIntradayOHLCIndexArray(exchange,symbol,cumnCandleIndexArray);

                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                });

            });


        });

    }

    private void loadHistoryOHLCData() {
        SharedMethods.getExchangeList().forEach(exchange -> {
            List<String> folderList = FileSystemManager.getHistoryFolderList(exchange);

            folderList.forEach(folderName -> {
                String folderPath = FileSystemManager.getHistoryFolderPathWithExchange(exchange) + System.getProperty("file.separator") + folderName;
                List<String> csvFiles = FileSystemManager.getCSVFileNamesList(folderPath);

                csvFiles.forEach(csvFile -> {
                    try {
                        String csvPath = folderPath + System.getProperty("file.separator") + csvFile;
                        String mappingSymbol = FileSystemManager.removeExtension(csvFile);
                        String symbol = SymbolMappingStore.getSharedInstance().getSymbol(exchange, mappingSymbol);
                        List<String> lines = FileSystemManager.getRowListOfFile(csvPath);

                        lines.forEach(line -> {
                            try {
                                if (!SharedMethods.isNullOrEmptyString(symbol)) {
                                    String[] dataAry = line.split("\\|");
                                    Candle candle = new Candle(symbol, SharedMethods.stringDateToLong(dataAry[1]), Double.parseDouble(dataAry[2]), Double.parseDouble(dataAry[3]), Double.parseDouble(dataAry[4]), Double.parseDouble(dataAry[5]), Long.parseLong(dataAry[6]), Double.parseDouble(dataAry[7]), Integer.parseInt(dataAry[8]), Double.parseDouble(dataAry[9]));
                                    OHLCHistoryDataStore.getSharedInstance().addHistryOHLCObject(exchange, symbol, candle);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });

            });

        });
    }


    private void buildTheSymbolMap() {

        SharedMethods.getExchangeList().forEach(exchange -> {
            try {
                List<String> lines = FileSystemManager.getRowListOfFile(FileSystemManager.getSymbolMappingFilePath(exchange) + System.getProperty("file.separator") + FileSystemManager.getSymbolMappingsFileName());
                lines.forEach(line -> {
                    String[] ary;
                    try {
                        ary = line.split("\\|");
                        SymbolMappingStore.getSharedInstance().addSymbolsToStore(exchange, ary[0], ary[1]);
                    } catch (Exception e) {
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        });

    }

    private long intradayMilliseconds(String timeStamp) {
        return Long.parseLong(timeStamp) * 1000; //convert in to milli seconds
    }
}
