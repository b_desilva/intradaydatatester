package com.dfn.store;

import java.util.HashMap;

public class SymbolMappingStore {
    private HashMap<String, HashMap<String, String>> symbolMap; // Exchange -> Mapping Code -> Symbol

    private static SymbolMappingStore self;

    private SymbolMappingStore() {
        symbolMap = new HashMap<>();
    }

    public static SymbolMappingStore getSharedInstance() {
        if (self == null) {
            self = new SymbolMappingStore();
        }
        return self;
    }

    public void addSymbolsToStore(String exchange, String mappingCode, String symbol) {
        if (symbolMap.containsKey(exchange)) {
            HashMap<String, String> codeMap = symbolMap.get(exchange);
            codeMap.put(mappingCode, symbol);
        } else {
            HashMap<String, String> codeMap = new HashMap<>();
            codeMap.put(mappingCode, symbol);
            symbolMap.put(exchange, codeMap);
        }

    }

    public String getSymbol(String exchange, String mappingCode) {
        if (symbolMap.containsKey(exchange)) {
            if (symbolMap.get(exchange).containsKey(mappingCode)) {
                return symbolMap.get(exchange).get(mappingCode);
            }
        }
        return "";
    }
}
