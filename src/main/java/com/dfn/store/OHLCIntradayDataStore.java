package com.dfn.store;

import com.dfn.store.object.Candle;

import java.util.*;

public class OHLCIntradayDataStore {

    private Map<String, Map<String, List<Candle>>> orginalDataList; //exchange-> symbol-> list // 1 min store
    private Map<String, Map<String, List<Candle[]>>> candleIndexDataList; //exchange-> symbol-> Candle index List // 1 min store


    private static OHLCIntradayDataStore self;

    private OHLCIntradayDataStore() {
        orginalDataList = new HashMap<>();
        candleIndexDataList = new HashMap<>();
    }

    public static OHLCIntradayDataStore getSharedInstance() {
        if (self == null) {
            self = new OHLCIntradayDataStore();
        }
        return self;
    }


    public void addIntradayOHLCObject(String exchange, String symbol, Candle candle) {
        if (orginalDataList.containsKey(exchange)) {
            Map<String, List<Candle>> symbolMap = orginalDataList.get(exchange);
            if (symbolMap.containsKey(symbol)) {
                List<Candle> candleList = symbolMap.get(symbol);
                addToList(candleList, candle);
            } else {
                List<Candle> candleList = new ArrayList<>();
                addToList(candleList, candle);
                symbolMap.put(symbol, candleList);
            }

        } else {
            HashMap<String, List<Candle>> symbolMap = new HashMap<>();
            List<Candle> candleList = new ArrayList<>();
            addToList(candleList, candle);
            symbolMap.put(symbol, candleList);
            orginalDataList.put(exchange, symbolMap);
        }
    }

    public void addIntradayOHLCIndexArray(String exchange, String symbol, Candle[] candle) {
        if (candleIndexDataList.containsKey(exchange)) {
            Map<String, List<Candle[]>> symbolMap = candleIndexDataList.get(exchange);
            if (symbolMap.containsKey(symbol)) {
                List<Candle[]> candleList = symbolMap.get(symbol);
                candleList.add(candle);
            } else {
                List<Candle[]> candleList = new ArrayList<>();
                candleList.add(candle);
                symbolMap.put(symbol, candleList);
            }

        } else {
            HashMap<String, List<Candle[]>> symbolMap = new HashMap<>();
            List<Candle[]> candleList = new ArrayList<>();
            candleList.add(candle);
            symbolMap.put(symbol, candleList);
            candleIndexDataList.put(exchange, symbolMap);
        }
    }

    public List<Candle> getIntradayOHLCIntardayList(String exchange, String symbol) {
        if (orginalDataList.containsKey(exchange)) {
            Map<String, List<Candle>> symbolMap = orginalDataList.get(exchange);
            if (symbolMap.containsKey(symbol)) {
                return symbolMap.get(symbol);
            }
        }
        return new ArrayList<>();
    }

    public Map<String, List<Candle[]>> getIntradayExchangeOHLCIndexIntardayList(String exchange) {
        if (candleIndexDataList.containsKey(exchange)) {
            return candleIndexDataList.get(exchange);
        }
        return new HashMap<String, List<Candle[]>>();
    }

    public List<String> getSymbolList(String exchange) {
        List<String> sym = new ArrayList<>();
        if (orginalDataList.containsKey(exchange)) {
            Map<String, List<Candle>> symbolMap = orginalDataList.get(exchange);
            sym.addAll(symbolMap.keySet());
        }
        return sym;
    }

    boolean addToList(List<Candle> list, Candle record) {
        boolean isNewRecord = false;
        int location = Collections.binarySearch(list, record, new OHLCComparator());
        if (location < 0) {
            list.add((location + 1) * -1, record);
            isNewRecord = true;
        } else {
            list.set(location, record);
            isNewRecord = false;
        }
        return isNewRecord;
    }
}
