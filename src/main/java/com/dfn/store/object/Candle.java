package com.dfn.store.object;

public class Candle {

    private String symbol;
    private long time;
    private double open;
    private double high;
    private double low;
    private double close;
    private double turnover;
    private long volume;
    private int noTrades;
    private double vwap;

    public Candle(String symbol, long time) {
        this.symbol = symbol;
        this.time = time;
    }

    public Candle(String symbol, long time, double open, double high, double low, double close, long volume, double turnover, int noTrades, double vwap) {
        this(symbol, time);
        this.open = open;
        this.high = high;
        this.low = low;
        this.close = close;
        this.turnover = turnover;
        this.volume = volume;
        this.noTrades = noTrades;
        this.vwap = vwap;
    }

    public String getSymbol() {
        return symbol;
    }

    public long getTime() {
        return time;
    }

    public double getOpen() {
        return open;
    }

    public double getHigh() {
        return high;
    }

    public double getLow() {
        return low;
    }

    public double getClose() {
        return close;
    }

    public double getTurnover() {
        return turnover;
    }

    public long getVolume() {
        return volume;
    }

    public double getVwap() {
        return vwap;
    }

    public int getNoTrades() {
        return noTrades;
    }
}
