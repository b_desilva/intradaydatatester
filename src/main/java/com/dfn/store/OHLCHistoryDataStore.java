package com.dfn.store;

import com.dfn.store.object.Candle;

import java.util.*;

public class OHLCHistoryDataStore {

    private Map<String, Map<String, List<Candle>>> orginalDataList; //exchange-> symbol-> list // 1 day store

    private static OHLCHistoryDataStore self;

    private OHLCHistoryDataStore() {
        orginalDataList = new HashMap<>();
    }

    public static OHLCHistoryDataStore getSharedInstance() {
        if (self == null) {
            self = new OHLCHistoryDataStore();
        }
        return self;
    }


    public void addHistryOHLCObject(String exchange, String symbol, Candle candle) {
        if (orginalDataList.containsKey(exchange)) {
            Map<String, List<Candle>> symbolMap = orginalDataList.get(exchange);
            if (symbolMap.containsKey(symbol)) {
                List<Candle> candleList = symbolMap.get(symbol);
                addToList(candleList, candle);
            } else {
                ArrayList<Candle> candleList = new ArrayList<>();
                addToList(candleList, candle);
                symbolMap.put(symbol, candleList);
            }

        } else {
            Map<String, List<Candle>> symbolMap = new HashMap<>();
            ArrayList<Candle> candleList = new ArrayList<>();
            addToList(candleList, candle);
            symbolMap.put(symbol, candleList);
            orginalDataList.put(exchange, symbolMap);
        }
    }

    public List<Candle> getHistryOHLCIntardayList(String exchange, String symbol) {
        if (orginalDataList.containsKey(exchange)) {
            Map<String, List<Candle>> symbolMap = orginalDataList.get(exchange);
            if (symbolMap.containsKey(symbol)) {
                return symbolMap.get(symbol);
            }
        }
        return new ArrayList<>();
    }


    public List<String> getSymbolList(String exchange) {
        List<String> sym = new ArrayList<>();
        if (orginalDataList.containsKey(exchange)) {
            Map<String, List<Candle>> symbolMap = orginalDataList.get(exchange);
            sym.addAll(symbolMap.keySet());
        }
        return sym;
    }

    public Candle getCandle(String exchange, String symbol, long time) {
        List<Candle> list = getHistryOHLCIntardayList(exchange, symbol);
        Candle tempRecord = new Candle(symbol, time);
        int location = Collections.binarySearch(list, tempRecord, new OHLCComparator());
        if (location >= 0) {
            return list.get(location);
        }
        return null;
    }


    private void addToList(List<Candle> list, Candle record) {
        int location = Collections.binarySearch(list, record, new OHLCComparator());
        if (location < 0) {
            list.add((location + 1) * -1, record);
        } else {
            list.set(location, record);
        }
    }


}
