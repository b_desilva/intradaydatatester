package com.dfn.store;

import com.dfn.store.object.Candle;

import java.util.Comparator;

public class OHLCComparator implements Comparator<Candle> {

    @Override
    public int compare(Candle do1, Candle do2) {
        if (do1.getTime() < do2.getTime()) return 1;
        if (do1.getTime() > do2.getTime()) return -1;
        return 0;

    }
}
