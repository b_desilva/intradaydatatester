package com.dfn;

import com.dfn.dataWriter.CSVDataWriter;
import com.dfn.shared.FileSystemManager;
import com.dfn.store.ArchiveDataStoreBuilder;

public class MainClass {

    public static void main(String[] args) {
        ArchiveDataStoreBuilder.getSharedInstance().startStoreBuilder();
        FileSystemManager.clearAllGenerateFolders();

        String exchange = "TDWL";
        int dayCount = 5;

        FileSystemManager.createExchangeFolders(exchange);
        CSVDataWriter.getSharedInstance().writeAverageVolumeTrade(exchange, dayCount);
        CSVDataWriter.getSharedInstance().writeVwapCrosses(exchange);
        CSVDataWriter.getSharedInstance().writeVwapLastCross(exchange);

        System.out.println("-----Finish the Generation-----");
    }

}
